﻿namespace SharedLibrary
{
    public class SimplifiedUser
    {
        public long Id { get; set; }
        public string Nick { get; set; }
        public string Username { get; set; }
        public string? Description { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
    }
}
