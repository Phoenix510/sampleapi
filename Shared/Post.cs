﻿using System.ComponentModel.DataAnnotations;

namespace SharedLibrary
{
    public partial class Post: Message
    {
        [Required(ErrorMessage = $"Title is required.")]
        [MaxLength(42, ErrorMessage = "Title cannot be longer than 42 chars")]
        [MinLength(3, ErrorMessage = "Title cannot be shorter than 3 chars")]
        public string Title { get; set; }
    }
}
