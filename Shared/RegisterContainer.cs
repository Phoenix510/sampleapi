﻿using System.ComponentModel.DataAnnotations;

namespace SharedLibrary
{
    public class RegisterContainer
    {
        [Required(ErrorMessage = $"Nick is required.")]
        [MinLength(6, ErrorMessage = "Nick cannot be shorter than 6 chars")]
        [MaxLength(24, ErrorMessage = "Nick cannot be longer than 24 chars")]
        public string Nick { get; set; }

        [Required(ErrorMessage = "Username is required.")]
        [MinLength(6, ErrorMessage = "Username cannot be shorter than 6 chars")]
        [MaxLength(24, ErrorMessage = "Username cannot be longer than 24 chars")]
        public string Username { get; set; }

        [EmailAddress]
        [MaxLength(256)]
        [Required(ErrorMessage = "Email is required.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [MinLength(8, ErrorMessage = "Nick cannot be shorter than 8 chars")]
        [MaxLength(64, ErrorMessage = "Password cannot be longer than 64 chars")]
        public string Password { get; set; }

    }
}
