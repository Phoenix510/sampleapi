﻿using System.ComponentModel.DataAnnotations;

namespace SharedLibrary
{
    public class Message
    {
        public long Id { get; set; }
        public DateTime CreatedDate { get; set; }

        [Required(ErrorMessage = $"Content is required.")]
        [MaxLength(360, ErrorMessage = "Content cannot be longer than 320 chars")]
        [MinLength(3, ErrorMessage = "Content cannot be shorter than 3 chars")]
        public string Content { get; set; }

        public PostCreator Creator { get; set; } = new PostCreator();
        public List<Message> Components { get; set; } = new List<Message>();
    }
}
