﻿using System.ComponentModel.DataAnnotations;

namespace SharedLibrary
{
    public class Credentials
    {
        [Required(ErrorMessage ="Password is required.")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Username is required.")]
        public string Username { get; set; }
    }
}
