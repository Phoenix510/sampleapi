﻿using Microsoft.AspNetCore.Components;
using SampleBlazorClient.Authentication;
using System.Text;

namespace SampleBlazorClient.Services
{
    public class WebConnectionService
    {
        private readonly HttpClient client;
        private readonly AuthService authService;
        private readonly NavigationManager navigationManager;

        public WebConnectionService(HttpClient client, AuthService authService, NavigationManager navigationManager)
        {
            this.client = client;
            this.authService = authService;
            this.navigationManager = navigationManager;
        }

        public async Task<(string Content, int StatusCode)> PostAsync(string URI, string toSendAsJson)
        {
            var content = new StringContent(toSendAsJson, Encoding.UTF8, "application/json");
            var result = await client.PostAsync(URI, content);
            if (result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                Logout();
                return (string.Empty, (int)result.StatusCode);
            }

            return (await result.Content.ReadAsStringAsync(), (int)result.StatusCode);
        }

        public async Task<(string Content, int StatusCode)> GetAsync(string URI)
        {
            var result = await client.GetAsync(URI);
            if (result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                Logout();
                return (string.Empty, (int)result.StatusCode);
            }

           return (await result.Content.ReadAsStringAsync(), (int)result.StatusCode);
        }

        private async void Logout()
        {
            await authService.Logout();
            navigationManager.NavigateTo("/login/logout", false);
        }
    }
}
