﻿namespace SampleBlazorClient.Services.Storage
{
    public interface Storage<T>
    {
        public T Get(string key);
        public IEnumerable<T> GetAll();
        public void Add(T item);
    }
}
