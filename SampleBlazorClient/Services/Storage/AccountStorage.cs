﻿using Blazored.SessionStorage;
using SampleBlazorClient.Services.APIRelated;
using SharedLibrary;

namespace SampleBlazorClient.Services.Storage
{
    public class AccountStorage
    {
        private readonly APIProxy apiService;
        private readonly ISessionStorageService sessionStorage;
        const string detailsKey = "accountDetails";

        public AccountStorage(APIProxy apiService, ISessionStorageService sessionStorage)
        {
            this.apiService = apiService;
            this.sessionStorage = sessionStorage;
        }

        public async Task<SimplifiedUser> Get()
        {
            var cachedUser = await sessionStorage.GetItemAsync<SimplifiedUser>(detailsKey);
            if (cachedUser == null)
                cachedUser = await GetFromAPI();
            
            return cachedUser;
        }

        private async Task<SimplifiedUser> GetFromAPI()
        {
            var accountDetails = await apiService.GetUserDetails();
            await sessionStorage.SetItemAsync<SimplifiedUser>(detailsKey, accountDetails);
            return accountDetails;
        }
    }
}
