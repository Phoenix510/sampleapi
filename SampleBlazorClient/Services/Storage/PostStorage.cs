﻿using Blazored.SessionStorage;
using SampleBlazorClient.Models;
using SampleBlazorClient.Services.APIRelated;

namespace SampleBlazorClient.Services.Storage
{
    public class PostStorage
    {
        private readonly APIProxy apiService;
        private readonly ISessionStorageService sessionStorage;
        const string postsKey = "posts";

        public PostStorage(APIProxy apiService, ISessionStorageService sessionStorage)
        {
            this.apiService = apiService;
            this.sessionStorage = sessionStorage;
        }

        public async Task<Post> Add(Post newPost)
        {
            Post? result = await apiService.AddPost(newPost);
            if (result is null)
                return null;

            var cachedPosts = await GetPostsFromStorage();
            SavePostsToStorage(cachedPosts.Prepend(result));

            return result;
        }

        public async Task<IEnumerable<Post>> GetAll()
        {
            var cachedPosts = await GetPostsFromStorage();

            if (cachedPosts.Count() < 1)
            {
                cachedPosts = await apiService.GetPosts();
                SavePostsToStorage(cachedPosts);
            }
            else
            {
                var newPosts = await apiService.GetNewerThan(cachedPosts.OrderBy(p => p.Id).Last().Id);
                if (newPosts.Count() < 1)
                    return cachedPosts;

                foreach(var post in newPosts) { cachedPosts.Prepend(post); }
                cachedPosts.OrderByDescending(p => p.Id);
                SavePostsToStorage(cachedPosts);
            }

            return cachedPosts;
        }

        private async Task<IEnumerable<Post>> GetPostsFromStorage()
        {
            var cachedPosts = await sessionStorage.GetItemAsync<IEnumerable<Post>>(postsKey);
            if (cachedPosts is null)
                cachedPosts = new Post[0];

            return cachedPosts;
        }

        private async void SavePostsToStorage(IEnumerable<Post> posts)
        {
            if (posts.Count() > 80)
                posts = posts.Take(80);
            await sessionStorage.SetItemAsync(postsKey, posts);
        }
    }
}
