﻿using Blazored.SessionStorage;
using Microsoft.AspNetCore.Components;
using SampleBlazorClient.Authentication;
using SampleBlazorClient.Services.APIRelated;
using SharedLibrary;
using System.Net.Http.Headers;
using System.Text.Json;

namespace SampleBlazorClient.Services
{
    public class APIServiceWithRefreshCheck : APIProxy
    {
        private readonly WebConnectionService webConn;
        private readonly APIService aPIService;
        private readonly HttpClient httpClient;
        private readonly ISessionStorageService sessionStorage;

        public APIServiceWithRefreshCheck(HttpClient client, AuthService authService, NavigationManager navigationManager, ISessionStorageService sessionStorage)
        {
            webConn = new WebConnectionService(client, authService, navigationManager);
            aPIService = new APIService(client, authService, navigationManager);
            httpClient = client;
            this.sessionStorage = sessionStorage;
        }

        public async Task<Models.Post> AddPost(Models.Post post)
        {
            await RefreshTokenIfNecessary();
            return await aPIService.AddPost(post);
        }

        public async Task<IEnumerable<Models.Post>> GetNewerThan(long id)
        {
            await RefreshTokenIfNecessary();
            return await aPIService.GetNewerThan(id);
        }

        public async Task<IEnumerable<Models.Post>> GetPosts(int numberOfPage = 0)
        {
            await RefreshTokenIfNecessary();
            return await aPIService.GetPosts(numberOfPage);
        }

        public async Task<SimplifiedUser> GetUserDetails()
        {
            await RefreshTokenIfNecessary();
            return await aPIService.GetUserDetails();
        }


        private async Task<bool> RefreshTokenIfNecessary()
        {
            var expirationDate = await sessionStorage.GetItemAsync<DateTime?>("tokenExpires");
            if(expirationDate is null || expirationDate > DateTime.UtcNow)
                return false;

            var refreshToken = await sessionStorage.GetItemAsStringAsync("refrToken");
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", refreshToken);
            var result = await webConn.GetAsync("https://localhost:7060/api/accounts/RefreshToken");

            if (result.StatusCode != 200)
                throw new Exception("Cannot refresh token!");

            var tokenModel = JsonSerializer.Deserialize<TokenModel>(result.Content, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            await sessionStorage.SetItemAsStringAsync("authToken", tokenModel.AccessToken);
            await sessionStorage.SetItemAsStringAsync("refrToken", tokenModel.RefreshToken);
            await sessionStorage.SetItemAsync<DateTime>("tokenExpires", tokenModel.Expires);

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);
            return true;
        }
    }
}
