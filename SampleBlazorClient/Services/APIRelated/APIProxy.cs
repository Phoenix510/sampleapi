﻿using SampleBlazorClient.Models;

namespace SampleBlazorClient.Services.APIRelated
{
    public interface APIProxy
    {
        public Task<Post> AddPost(Post post);

        public Task<IEnumerable<Post>> GetPosts(int numberOfPage = 0);

        public Task<IEnumerable<Post>> GetNewerThan(long id);

        public Task<SharedLibrary.SimplifiedUser> GetUserDetails();
    }
}
