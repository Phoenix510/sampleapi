﻿using Microsoft.AspNetCore.Components;
using System.Text.Json;
using SampleBlazorClient.Authentication;
using SampleBlazorClient.Models;
using SampleBlazorClient.Services.APIRelated;

namespace SampleBlazorClient.Services
{
    public class APIService: APIProxy
    {
        private readonly WebConnectionService webConn;

        public APIService(HttpClient client, AuthService authService, NavigationManager navigationManager)
        {
            webConn = new WebConnectionService(client, authService, navigationManager);
        }

        public async Task<Post> AddPost(Post post)
        {
            var result = await webConn.PostAsync("https://localhost:7060/api/posts/Create", JsonSerializer.Serialize(post));
            if (result.StatusCode != 201)
                return null;

            var newPost = JsonSerializer.Deserialize<Post>(result.Content, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            return newPost;
        }

        public async Task<IEnumerable<Post>> GetPosts(int numberOfPage = 0)
        {
            var result = await webConn.GetAsync($"https://localhost:7060/api/posts/Get/{numberOfPage}");
            if(result.StatusCode != 200)
                return null;
            var items = JsonSerializer.Deserialize<Post[]>(result.Content, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });
            
            return items;
        }

        public async Task<IEnumerable<Post>> GetNewerThan(long id)
        {
            var result = await webConn.GetAsync($"https://localhost:7060/api/posts/GetNewerThan/{id}");
            if (result.StatusCode != 200)
                return null;
            var items = JsonSerializer.Deserialize<Post[]>(result.Content, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            return items;
        }

        public async Task<SharedLibrary.SimplifiedUser> GetUserDetails()
        {
            var result = await webConn.GetAsync("https://localhost:7060/api/accounts/details");
            if (result.StatusCode != 200)
                return null;

            var userDetails = JsonSerializer.Deserialize<SharedLibrary.SimplifiedUser>(result.Content, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            return userDetails;
        }
    }
}
