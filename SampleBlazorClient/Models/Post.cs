﻿using Microsoft.AspNetCore.Components;
using SampleBlazorClient.Components;
using System.ComponentModel.DataAnnotations;

namespace SampleBlazorClient.Models
{
    public class Post : Message
    {
        [Required(ErrorMessage = $"Title is required.")]
        [MaxLength(42, ErrorMessage = "Title cannot be longer than 42 chars")]
        [MinLength(3, ErrorMessage = "Title cannot be shorter than 3 chars")]
        public string Title { get; set; }

        public override RenderFragment CreateDynamicComponent(string classValue = "") => builder =>
        {
            builder.OpenComponent(0, typeof(MessageTemplate));
            builder.AddAttribute(1, "Id", this.Id);
            builder.AddAttribute(2, "BodyRender", new RenderFragment[] { CreateTitleComponent(),  CreateBodyComponent(classValue)});
            builder.AddAttribute(3, "Class", classValue);
            builder.CloseComponent();
        };

        private RenderFragment CreateTitleComponent(string classValue = "") => builder => 
        { 
            builder.OpenComponent(0, typeof(TitleComponent));
            builder.AddAttribute(1, "Title", Title);
            builder.AddAttribute(2, "Class", classValue);
            builder.CloseComponent();
        };
    }
}
