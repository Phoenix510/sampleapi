﻿using Microsoft.AspNetCore.Components;
using SampleBlazorClient.Components;
using System.ComponentModel.DataAnnotations;

namespace SampleBlazorClient.Models
{
    public partial class Message
    {
        public long Id { get; set; }
        public DateTime CreatedDate { get; set; }

        [Required(ErrorMessage = $"Content is required.")]
        [MaxLength(360, ErrorMessage = "Content cannot be longer than 320 chars")]
        [MinLength(3, ErrorMessage = "Content cannot be shorter than 3 chars")]
        public string Content { get; set; }

        public SharedLibrary.PostCreator Creator { get; set; } = new SharedLibrary.PostCreator();
        public List<Message> Components { get; set; } = new List<Message>();

        public IEnumerable<RenderFragment> GetComponentsRenders(string classValue = "")
        {
            var listOfRenders = new List<RenderFragment>();
            foreach(var component in Components)
            {
                listOfRenders.Add(component.CreateDynamicComponent(classValue));
            }
            return listOfRenders;
        }

        public virtual RenderFragment CreateDynamicComponent(string classValue = "") => builder =>
        {
            builder.OpenComponent(0, typeof(MessageTemplate));
            builder.AddAttribute(1, "Id", this.Id);
            builder.AddAttribute(2, "BodyRender", new RenderFragment[] { CreateBodyComponent() });
            builder.AddAttribute(3, "Class", classValue);
            builder.CloseComponent();
        };


        protected RenderFragment CreateBodyComponent(string classValue = "") => builder =>
        {
            builder.OpenComponent(0, typeof(MessageComponent));
            builder.AddAttribute(1, "Item", this);
            builder.AddAttribute(2, "Class", classValue);
            builder.CloseComponent();
        };

    }
}
