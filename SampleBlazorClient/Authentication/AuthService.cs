﻿using Blazored.LocalStorage;
using Blazored.SessionStorage;
using Microsoft.AspNetCore.Components.Authorization;
using SharedLibrary;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace SampleBlazorClient.Authentication
{

    public class AuthService
    {
        private readonly HttpClient client;
        private readonly AuthenticationStateProvider authenticationStateProvider;
        private readonly ISessionStorageService sessionStorage;

        public AuthService(HttpClient client, AuthenticationStateProvider authenticationStateProvider,
            ISessionStorageService sessionStorage)
        {
            this.client = client;
            this.authenticationStateProvider = authenticationStateProvider;
            this.sessionStorage = sessionStorage;
        }

        public async Task<(string, int)> Login(Credentials credentials)
        {
            var result = await client.PostAsync("https://localhost:7060/api/accounts/login",
                new StringContent(JsonSerializer.Serialize(credentials), Encoding.UTF8, "application/json"));

            if (!result.IsSuccessStatusCode)
                return (null, (int)result.StatusCode);

            var content = await result.Content.ReadAsStringAsync();
            var tokenModel = JsonSerializer.Deserialize<TokenModel>(content, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            await sessionStorage.SetItemAsStringAsync("authToken", tokenModel.AccessToken);
            await sessionStorage.SetItemAsStringAsync("refrToken", tokenModel.RefreshToken);
            await sessionStorage.SetItemAsync<DateTime>("tokenExpires", tokenModel.Expires);
            ((AuthStateProvider)authenticationStateProvider).NotifyUserAuthentication(tokenModel.AccessToken);

            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            return (content, 200);
        }

        public async Task Logout()
        {
            await sessionStorage.RemoveItemAsync("authToken");
            ((AuthStateProvider)authenticationStateProvider).NotifyUserLogout();
            client.DefaultRequestHeaders.Authorization = default;
        }


        public async Task<(string, int)> Register(RegisterContainer registerContainer)
        {
            var result = await client.PostAsync("https://localhost:7060/api/accounts/register",
                new StringContent(JsonSerializer.Serialize(registerContainer), Encoding.UTF8, "application/json"));

            var content = await result.Content.ReadAsStringAsync();

            if (!result.IsSuccessStatusCode)
                return (content, (int)result.StatusCode);

            return (content, 200);
        }
    }
}
