﻿using Blazored.LocalStorage;
using Blazored.SessionStorage;
using Microsoft.AspNetCore.Components.Authorization;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace SampleBlazorClient.Authentication
{
    public class AuthStateProvider : AuthenticationStateProvider
    {
        private readonly HttpClient client;
        private readonly ISessionStorageService sessionStorage;
        private readonly AuthenticationState anonymous;

        public AuthStateProvider(HttpClient client, ISessionStorageService sessionStorage)
        {
            this.client = client;
            this.sessionStorage = sessionStorage;
            anonymous = new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
           var token = await sessionStorage.GetItemAsync<string>("authToken");
           if (string.IsNullOrEmpty(token))
                return anonymous;

            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            return new AuthenticationState(
                new ClaimsPrincipal(new ClaimsIdentity(JwtParser.ParseClaimsFromJWT(token), "jwtAuthType")));
        }

        public void NotifyUserAuthentication(string token)
        {
            var authUser = new ClaimsPrincipal(new ClaimsIdentity(JwtParser.ParseClaimsFromJWT(token), "jwtAuthType"));
            var state = Task.FromResult(new AuthenticationState(authUser));
            NotifyAuthenticationStateChanged(state);
        }

        public void NotifyUserLogout()
        {
            var state = Task.FromResult(anonymous);
            NotifyAuthenticationStateChanged(state);
        }
    }
}
