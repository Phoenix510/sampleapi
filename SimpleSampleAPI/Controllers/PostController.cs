﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimpleSampleAPI.Models;
using SimpleSampleAPI.Repositories;
using SimpleSampleAPI.Services.Translator;
using System.Security.Claims;

namespace SimpleSampleAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class PostsController : Controller
    {
        private readonly PostRepository _postRepository;
        private readonly UserRepository _userRepository;

        public PostsController(PostRepository postRepository, UserRepository userRepository)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get() => Ok(PostEntityPostTranslator.GetTranslator().Translate(await _postRepository.Get()));

        [AllowAnonymous]
        [HttpGet("{pageNumber}")]
        public async Task<IActionResult> Get(int pageNumber) => Ok(PostEntityPostTranslator.GetTranslator().Translate(await _postRepository.Get(40, pageNumber)));

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetNewerThan(long id) => Ok(PostEntityPostTranslator.GetTranslator().Translate(await _postRepository.GetNewerThan(id)));

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> Details(long id)
        {
            return Ok(PostEntityPostTranslator.GetTranslator().Translate(await _postRepository.Get(id)));
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create([FromBody] SharedLibrary.Post post)
        {
            string? nick = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            if (nick is null)
                return Unauthorized();

            Post postToAdd = new Post(post);
            User? userFromDB = await _userRepository.GetByNick(nick);
            postToAdd.User = userFromDB;
            postToAdd.UserId = userFromDB.Id;

            return StatusCode(201, await _postRepository.Add(postToAdd));
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(long id, Post post)
        {
            if (id != post.Id)
            {
                return NotFound();
            }

            return Ok(await _postRepository.Update(post));
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            if (await _postRepository.Delete(await _postRepository.Get(id)) == default)
                return NotFound();
            return Ok();
        }
    }
}
