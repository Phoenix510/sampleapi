﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SimpleSampleAPI.Data;
using SimpleSampleAPI.Models;
using SimpleSampleAPI.Repositories;
using System.Security.Claims;

namespace SimpleSampleAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    public class UsersController : Controller
    {
        private readonly UserRepository _userRepository;

        public UsersController(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get() => Ok(await _userRepository.GetUsers());

        [HttpGet("{id}")]
        public async Task<IActionResult> Details(long id)
        {
            User? userFromDB = await _userRepository.Get(id);
            if(userFromDB is null)
                return NotFound();
            return Ok(userFromDB);
        }

        [HttpPost]
        public async Task<IActionResult> Create([Bind("Nick,Username,Description,Email")] User user)
        {
            return StatusCode(201, await _userRepository.Add(user));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Nick,Username,Description,Email")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            return Ok(await _userRepository.Update(user));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            if(await _userRepository.Delete(await _userRepository.Get(id)) == default)
                return NotFound();
            return Ok();
        }
    }
}
