﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimpleSampleAPI.Managers;
using SimpleSampleAPI.Models;
using SimpleSampleAPI.Repositories;
using SimpleSampleAPI.Services;
using SimpleSampleAPI.Services.Translator;
using System.Security.Claims;

namespace SimpleSampleAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AccountsController : Controller
    {
        private readonly UserRepository _userRepository;
        private readonly IConfiguration _configuration;

        public AccountsController(UserRepository userRepository, IConfiguration config)
        {
            _userRepository = userRepository;
            _configuration = config;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]SharedLibrary.Credentials credentials)
        {
            var user = await _userRepository.GetByUsername(credentials.Username);
            if (user is null)
                return Unauthorized();
            if (!AccountManager.AuthenticateAccount(user, credentials.Password)) 
                return Unauthorized();

            var tokenModel = JWTManager.GenerateTokens(user, _configuration);
            return Ok(tokenModel);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]SharedLibrary.RegisterContainer toRegister)
        {
            var user = new User(toRegister);

            (bool success, string reason) verificationResult = await VerificationService.VerifyUserData(user, _userRepository);
            if (!verificationResult.success)
                return BadRequest(verificationResult.reason);
            var newUser = AccountManager.CreateAccount(user);
            if (newUser is null)
                return StatusCode(400, "Unknown");

            await _userRepository.Add(newUser);
            return Ok();
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> RefreshToken()
        {
            string? tokenType = User.Claims.FirstOrDefault(x => x.Type == "TokenType")?.Value;
            if (tokenType != "Refresh")
                return Unauthorized();

            string? nick = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            if (nick is null)
                return Unauthorized();

            var user = await _userRepository.GetByNick(nick);
            if (user is null)
                return Unauthorized();

            var tokenModel = JWTManager.GenerateTokens(user, _configuration);
            return Ok(tokenModel);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Details()
        {
            string? nick = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            if (nick is null)
                return Unauthorized();
            User? userFromDB = await _userRepository.GetByNick(nick);
            return Ok(SimplifiedUserUserTranslator.GetTranslator().Translate(userFromDB));
        }
    }
}
