﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SimpleSampleAPI.Models;

namespace SimpleSampleAPI.Data
{
    public class SimpleSampleAPIContext : DbContext
    {
        public SimpleSampleAPIContext (DbContextOptions<SimpleSampleAPIContext> options)
            : base(options)
        {
           
        }

        public DbSet<SimpleSampleAPI.Models.User> User { get; set; }
        public DbSet<SimpleSampleAPI.Models.Post> Post { get; set; }
        public DbSet<SimpleSampleAPI.Models.Role> Role { get; set; }
    }
}
