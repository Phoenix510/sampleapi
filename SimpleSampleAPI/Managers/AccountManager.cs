﻿using SimpleSampleAPI.Models;
using SimpleSampleAPI.Services;
using SimpleSampleAPI.Services.Translator;

namespace SimpleSampleAPI.Managers
{
    public static class AccountManager
    {
        public static bool AuthenticateAccount(User toCheck, string password)
        {
            if (!BCrypt.Net.BCrypt.Verify(password, toCheck.Password))
                return false;

            return true;
        }

        public static User CreateAccount(User baseData)
        {
            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(baseData.Password);
            var user = new User(SimplifiedUserUserTranslator.GetTranslator().Translate(baseData), hashedPassword);
            return user;
        }
    }
}
