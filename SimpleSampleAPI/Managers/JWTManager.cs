﻿using Microsoft.IdentityModel.Tokens;
using SharedLibrary;
using SimpleSampleAPI.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace SimpleSampleAPI.Managers
{
    public class JWTManager
    {
        public static TokenModel GenerateTokens(User user, IConfiguration _config)
        {
            return new TokenModel() { AccessToken = GenerateAccessToken(user, _config),
                RefreshToken = GenerateRefreshToken(user, _config), Expires =  DateTime.UtcNow.AddMinutes(10) };
        }

        public static string GenerateAccessToken(User user, IConfiguration _config)
        {
            return GenerateJWT(GenerateClaims(user), DateTime.Now.AddMinutes(10), _config);
        }

        public static string GenerateRefreshToken(User user, IConfiguration _config)
        {
            var claims = GenerateClaims(user).ToList();
            claims.Add(new Claim("TokenType", "Refresh"));
            return GenerateJWT(claims, DateTime.Now.AddMinutes(12), _config);
        }

        private static IEnumerable<Claim> GenerateClaims(User user)
        {
            Claim[] claims = new[] {
                new Claim(ClaimTypes.Name, user.Nick),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var listOfClaims = claims.ToList();
            listOfClaims.AddRange(GenerateRolesClaim(user.Roles));

            return listOfClaims;
        }

        private static Claim[] GenerateRolesClaim(IEnumerable<Role> roles)
        {
            if (roles is null || roles.Count() < 1)
                return new Claim[] { new Claim(ClaimTypes.Role, "User") };

            var claims = new Claim[roles.Count()];
            int i = 0;
            foreach (var role in roles)
            {
                claims[i] = new Claim(ClaimTypes.Role, role.Name.ToString());
                i++;
            }

            return claims;
        }

        private static string GenerateJWT(IEnumerable<Claim> claims, DateTime expires, IConfiguration _config)
        {
            var securityKey = new SymmetricSecurityKey(Convert.FromBase64String(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                claims,
                expires: expires,
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
