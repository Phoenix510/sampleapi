﻿using SimpleSampleAPI.Models;
using SimpleSampleAPI.Repositories;

namespace SimpleSampleAPI.Services
{
    public class VerificationService
    {
        public static async Task<(bool success, string reason)> VerifyUserData(User userToVerify, 
            UserRepository userRepository)
        {
            //TODO verify with regex
            if (await userRepository.GetByUsername(userToVerify.Username) is not null)
                return (false, "Username taken");
            if (await userRepository.GetByNick(userToVerify.Nick) is not null)
                return (false, "Nick taken");
            if (await userRepository.GetByEmail(userToVerify.Email) is not null)
                return (false, "Email taken");

            return (true, string.Empty);
        }
    }
}
