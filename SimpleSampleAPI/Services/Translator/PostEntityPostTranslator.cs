﻿using SharedLibrary;

namespace SimpleSampleAPI.Services.Translator
{
    public class PostEntityPostTranslator : ITranslator<SharedLibrary.Post, Models.Post>
    {
        private PostEntityPostTranslator() { }

        private static PostEntityPostTranslator? _instance = null;

        private static readonly object _lock = new object();

        public static PostEntityPostTranslator GetTranslator()
        {
            if (_instance == null)
            {
                lock (_lock)
                    _instance = new PostEntityPostTranslator();
            }

            return _instance;
        }

        public SharedLibrary.Post Translate(Models.Post item)
        {
            var outputPost = new SharedLibrary.Post()
            {
                Id = item.Id,
                CreatedDate = item.CreatedDate,
                Creator = item.Creator,
                Content = item.Content,
                Title = item.Title
            };

            outputPost.Components = CommentsToMessages(item.Comments).ToList();

            return outputPost;
        }

        public IEnumerable<SharedLibrary.Post> Translate(IEnumerable<Models.Post> posts)
        {
            var toReturn = new List<SharedLibrary.Post>();
            foreach (var post in posts)
            {
                if(post.PostId is null)
                    toReturn.Add(Translate(post));
            }

            return toReturn;
        }

        public IEnumerable<Message> CommentsToMessages(IEnumerable<Models.Post> posts)
        {
            var toReturn = new List<Message>();
            foreach (var post in posts)
            {
                var outputMessage = new Message()
                {
                    Id = post.Id,
                    CreatedDate = post.CreatedDate,
                    Creator = post.Creator,
                    Content = post.Content,
                };

                outputMessage.Components.AddRange(CommentsToMessages(post.Comments));
                toReturn.Add(outputMessage);
            }
            return toReturn;
        }

        public Models.Post TranslateBack(SharedLibrary.Post item)
        {
            throw new NotImplementedException();
        }
    }
}
