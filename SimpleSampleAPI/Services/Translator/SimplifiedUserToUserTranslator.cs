﻿using SharedLibrary;
using SimpleSampleAPI.Models;

namespace SimpleSampleAPI.Services.Translator
{
    public class SimplifiedUserUserTranslator : ITranslator<SimplifiedUser, User>
    {
        private SimplifiedUserUserTranslator() { }

        private static SimplifiedUserUserTranslator? _instance = null;

        private static readonly object _lock = new object();

        public static SimplifiedUserUserTranslator GetTranslator()
        {
            if (_instance == null)
            {
                lock (_lock)
                    _instance = new SimplifiedUserUserTranslator();
            }

            return _instance;
        }

        public SimplifiedUser Translate(User item)
        {
            return new SimplifiedUser()
            {
                Id = item.Id,
                Nick = item.Nick,
                Username = item.Username,
                Description = item.Description,
                Email = item.Email,
            };
        }

        public User TranslateBack(SimplifiedUser item)
        {
            return new User()
            {
                Id = item.Id,
                Nick = item.Nick,
                Username = item.Username,
                Description = item.Description,
                Email = item.Email,
            };
        }
    }
}
