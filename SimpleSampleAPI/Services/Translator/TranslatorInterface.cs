﻿using SimpleSampleAPI.Models;

namespace SimpleSampleAPI.Services.Translator
{
    public interface ITranslator<T, K>
    {
        public T Translate(K item);
        public K TranslateBack(T item);
    }
}
