﻿using Microsoft.EntityFrameworkCore;
using SimpleSampleAPI.Data;
using SimpleSampleAPI.Repositories;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<SimpleSampleAPIContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("SimpleSampleAPIContext")));

var optionsBuilder = new DbContextOptionsBuilder<SimpleSampleAPIContext>();
optionsBuilder.UseSqlServer(builder.Configuration.GetConnectionString("SimpleSampleAPIContext"));
var context = new SimpleSampleAPIContext(optionsBuilder.Options);
builder.Services.AddSingleton(new UserRepository(context));
builder.Services.AddSingleton(new PostRepository(context));

builder.Services.AddCors(options =>
            {
    options.AddDefaultPolicy( //allows only blazor client origin
        builder => builder.WithOrigins("https://localhost:7291").AllowAnyHeader().WithMethods("GET", "POST", "PUT", "DELETE"));
});
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
              .AddJwtBearer(options =>
              {
                  options.TokenValidationParameters = new TokenValidationParameters
                  {
                      ValidateIssuer = true,
                      ValidateAudience = true,
                      ValidateLifetime = true,
                      ValidateIssuerSigningKey = true,
                      ValidIssuer = builder.Configuration["Jwt:Issuer"],
                      ValidAudience = builder.Configuration["Jwt:Issuer"],
                      IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String(builder.Configuration["Jwt:Key"]))
                  };
              });

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

app.UseAuthentication();
app.UseAuthorization();
app.UseCors();

app.MapControllers();


app.Run();
