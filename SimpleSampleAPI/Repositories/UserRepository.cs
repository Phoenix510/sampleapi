﻿using Microsoft.EntityFrameworkCore;
using SimpleSampleAPI.Data;
using SimpleSampleAPI.Models;

namespace SimpleSampleAPI.Repositories
{
    public class UserRepository: BaseRepository<User>
    {
        public UserRepository(SimpleSampleAPIContext context) : base(context)
        {
        }

        public async Task<IEnumerable<User>> GetUsers() => await _context.User.Include(u=> u.Roles).ToListAsync();
        public async override Task<User> Get(long Id) => await _context.User.Include(u => u.Roles).FirstOrDefaultAsync();

        public async Task<User> GetByUsername(string username) => await _context.User.Include(u => u.Roles).FirstOrDefaultAsync(u=>u.Username == username);
        public async Task<User> GetByNick(string nick) => await _context.User.Include(u => u.Roles).FirstOrDefaultAsync(u => u.Nick == nick);
        public async Task<User> GetByEmail(string email) => await _context.User.Include(u => u.Roles).FirstOrDefaultAsync(u=> u.Email == email);

        public async Task<IEnumerable<User>> Delete(IEnumerable<User> toDelete)
        {
            _context.User.RemoveRange(toDelete);
            await _context.SaveChangesAsync();
            return toDelete;
        }
    }
}
