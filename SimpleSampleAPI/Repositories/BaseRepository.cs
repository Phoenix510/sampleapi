﻿using SimpleSampleAPI.Data;
using SimpleSampleAPI.Models;

namespace SimpleSampleAPI.Repositories
{
    public abstract class BaseRepository<T> : Repository<T> where T : class, Entity, new()
    {
        protected readonly SimpleSampleAPIContext _context;

        public BaseRepository(SimpleSampleAPIContext context)
        {
            _context = context;
        }

        public async virtual Task<T> Add(T toAdd)
        {
            _context.Add(toAdd);
            await _context.SaveChangesAsync();
            return toAdd;
        }

        public async virtual Task<T> Get(long Id) => await _context.FindAsync<T>(Id) ?? new T();

        public async virtual Task<T> Update(T toUpdate)
        {
            if (toUpdate == null)
                return default;
            if(Get(toUpdate.Id) is null)
                return default;

            _context.Update(toUpdate);
            await _context.SaveChangesAsync();
            return toUpdate;
        }

        public async virtual Task<T> Delete(T toDelete)
        {
            if(toDelete == null)
                return default;
            _context.Remove(toDelete);
            await _context.SaveChangesAsync();
            return toDelete;
        }
    }
}
