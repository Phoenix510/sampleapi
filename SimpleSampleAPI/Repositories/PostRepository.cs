﻿using Microsoft.EntityFrameworkCore;
using SimpleSampleAPI.Data;
using SimpleSampleAPI.Models;

namespace SimpleSampleAPI.Repositories
{
    public class PostRepository : BaseRepository<Post>
    {
        public PostRepository(SimpleSampleAPIContext context) : base(context)
        {
        }

        // public async Task<IEnumerable<Post>> Get(int numberOfPosts = 40) => await _context.Post.OrderByDescending(p => p.Id).Take(numberOfPosts).Include(p => p.User).ToListAsync();
        public async Task<IEnumerable<Post>> Get(int numberOfPosts = 40, int numberOfPage = 0) =>
            await _context.Post.OrderByDescending(p => p.Id)
            .Skip(numberOfPage * numberOfPosts)
            .Take(numberOfPosts)
            .Include(p => p.User)
            .ToListAsync();

        public async Task<IEnumerable<Post>> GetNewerThan(long id, int numberOfPosts = 40) =>
            await _context.Post.OrderByDescending(p => p.Id)
            .Where(p => p.Id > id)
            .Take(numberOfPosts)
            .Include(p => p.User)
            .ToListAsync();


        public async override Task<Post> Get(long Id) => await _context.Post.Where(p => p.Id == Id)
            .Include(p => p.User).FirstOrDefaultAsync();

        public async override Task<Post> Add(Post toAdd)
        {
            toAdd.CreatedDate = DateTime.UtcNow;
            return await base.Add(toAdd);
        }
    }
}
