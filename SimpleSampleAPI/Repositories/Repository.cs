﻿namespace SimpleSampleAPI.Repositories
{
    public interface Repository<T>
    {
        public Task<T> Add(T toAdd);
        public Task<T> Get(long id);
        public Task<T> Update(T toUpdate);
        public Task<T> Delete(T toDelete);
    }
}
