﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SimpleSampleAPI.Models
{
    public class Post: Entity
    {
        public long Id { get; set; }

        [MaxLength(42, ErrorMessage = "Title cannot be longer than 42 chars")]
        [MinLength(3, ErrorMessage = "Title cannot be shorter than 3 chars")]
        public string? Title { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required(ErrorMessage = $"Content is required.")]
        [MaxLength(360, ErrorMessage = "Content cannot be longer than 320 chars")]
        [MinLength(3, ErrorMessage = "Content cannot be shorter than 3 chars")]
        public string Content { get; set; }

        [JsonIgnore]
        [ForeignKey("User")]
        public long UserId { get; set; }
        [Required]
        [JsonIgnore]
        public User User { get; set; } = new User();
        
        public HashSet<Post>? Comments { get; set; } = new HashSet<Post> { };
        public long? PostId { get; set; } 

        [NotMapped]
        public PostCreator Creator
        {
            get { return new PostCreator(User); }
        }


        public Post() { }
        public Post(SharedLibrary.Post post)
        {
            Id = post.Id;
            Title = post.Title;
            Content = post.Content;
        }
    }
}
