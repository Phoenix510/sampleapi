﻿using SharedLibrary;
using System.ComponentModel.DataAnnotations;

namespace SimpleSampleAPI.Models
{
    public enum RoleNames
    {
        User = 0,
        Administrator = 1
    }

    public class Role
    {
        public int Id { get; set; }
        public RoleNames Name { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }

    public class User : Entity
    {
        public long Id { get; set; }

        [Required]
        [MaxLength(24)]
        public string Nick { get; set; }

        [Required]
        [MaxLength(24)]
        public string Username { get; set; }

        [MaxLength(280)]
        public string? Description { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(256)]
        public string Email { get; set; }

        [Required]
        [MaxLength(500)]
        public string Password { get; set; }

        public virtual ICollection<Role> Roles { get; set; }

        public User()
        {
            Id = -1;
            Nick = string.Empty;
            Username = string.Empty;
            Description = string.Empty;
            Password = string.Empty;
            Email = string.Empty;
        }

        public User(string nick, string username, string password, string email, string description = "")
        {
            Nick = nick;
            Username = username;
            Description = description;
            Password = password;
            Email = email;
        }

        public User(SimplifiedUser simplifiedUser, string password)
        {
            Nick = simplifiedUser.Nick;
            Username = simplifiedUser.Username;
            Description = simplifiedUser.Description;
            Email = simplifiedUser.Email;
            Password = password;
        }


        public User(RegisterContainer registerContainer)
        {
            Nick = registerContainer.Nick;
            Username = registerContainer.Username;
            Email = registerContainer.Email;
            Password = registerContainer.Password;
        }
    }
}
