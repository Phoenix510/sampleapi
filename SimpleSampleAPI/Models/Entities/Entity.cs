﻿namespace SimpleSampleAPI.Models
{
    public interface Entity
    {
        public long Id { get; set; }
    }
}
