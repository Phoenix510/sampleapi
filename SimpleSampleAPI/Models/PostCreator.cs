﻿namespace SimpleSampleAPI.Models
{
    public class PostCreator: SharedLibrary.PostCreator
    {
        public PostCreator(User basedOn)
        {
            Id = basedOn.Id;
            Nick = basedOn.Nick;
        }
    }
}
