﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SimpleSampleAPI.Migrations
{
    public partial class postsInPosts2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Post",
                type: "nvarchar(42)",
                maxLength: 42,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(42)",
                oldMaxLength: 42);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Post",
                type: "nvarchar(42)",
                maxLength: 42,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(42)",
                oldMaxLength: 42,
                oldNullable: true);
        }
    }
}
